# TypeError: Can't instantiate abstract class Employee with abstract method calculate_payroll
import pytest
from employee import Employee

def test_employee_expects_typeerror():
    match='Can\'t instantiate abstract class Employee with abstract method calculate_payroll'
    with pytest.raises(expected_exception=TypeError, match=match):
        Employee(100, "name")
